﻿// Write your JavaScript code.
$(function () {
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        if (exdays == undefined) {
            exdays = 1;
        }
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        console.log("Set Cookie: " + cname + "=" + cvalue, expires);
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return false;
    }

    function checkCookie() {
        var user = getCookie("CookieAuthentication");
        if (user != "") {
            $("#login-button").show();
        } else {
            $("#display").hide();
        }
    }

    function newSession() {
        var sessionnumber = Math.random() * 10000000000.0;
        console.log("New Session: " + sessionnumber);
        setCookie("session", sessionnumber, 0.0125);
    }

    function checkSession() {
        if (getCookie("session") == false) {
            newSession();
        }
        console.log("Current Session: " + getCookie("session"));
    }

    window.onbeforeunload = function () {
        console.log("Closing - Expire 'CookieAuthentication' Cookie");
        setCookie("username", "", 0);
    };

    window.onload = checkCookie();
    window.onload = checkSession();
});


# peachpie-responsive-file-manager
Responsive File Manager running on .NET Core with Peachpie

## Getting Started

1. Get the ResponsiveFileManager NuGet package from: https://www.nuget.org/packages/ResponsiveFileManager/

2. Create the following class:

```csharp
public class ResponsiveFileManagerOptions
{
    /// <summary>
    /// Path from base_url to base of upload folder. Use start and final /
    /// </summary>
    public string UploadDirectory { get; set; }

    /// <summary>
    /// Relative path from filemanager folder to upload folder. Use final /
    /// </summary>
    public string CurrentPath { get; set; }

    /// <summary>
    /// Relative path from filemanager folder to thumbs folder. Use final / and DO NOT put inside upload folder.
    /// </summary>
    public string ThumbsBasePath { get; set; }
}
```

3. Add the following to your **appsettings.json**:

```json
"ResponsiveFileManagerOptions": {
    // Path from base_url to base of upload folder. Use start and final /
    "UploadDirectory": "/Media/Uploads/",

    // Relative path from filemanager folder to upload folder. Use final /
    "CurrentPath": "../Media/Uploads/",

    // Relative path from filemanager folder to thumbs folder. Use final / and DO NOT put inside upload folder.
    "ThumbsBasePath": "../Media/Thumbs/"
}
```

4. Open your `Startup.cs` and ensure it looks something like this:

```csharp
public void ConfigureServices(IServiceCollection services)
{
    // etc
	
    // Adds a default in-memory implementation of IDistributedCache.
    services.AddDistributedMemoryCache();

    services.AddSession(options =>
    {
        options.IdleTimeout = TimeSpan.FromMinutes(30);
        options.Cookie.HttpOnly = true;
    });
	
    // etc
}

public void Configure(IApplicationBuilder app)
{
    // etc

    app.UseSession();

    var rfmOptions = new ResponsiveFileManagerOptions();
    Configuration.GetSection("ResponsiveFileManagerOptions").Bind(rfmOptions);
	
    string root = Path.Combine(new FileInfo(Assembly.GetEntryAssembly().Location).DirectoryName, "wwwroot");

    app.UsePhp(new PhpRequestOptions(scriptAssemblyName: "ResponsiveFileManager")
    {
        BeforeRequest = (Context ctx) =>
        {
            ctx.Globals["appsettings"] = (PhpValue)new PhpArray()
            {
                { "upload_dir", rfmOptions.UploadDirectory },
                { "current_path", rfmOptions.CurrentPath },
                { "thumbs_base_path", rfmOptions.ThumbsBasePath }
            };
        }
    });

    app.UseDefaultFiles();
    app.UseStaticFiles(); // For default wwwroot location
    app.UseStaticFiles(new StaticFileOptions()
    {
        FileProvider = new PhysicalFileProvider(root)
    });
	
    // etc
}
```

You can use the source code in this repo, as follows:

1. Open the solution in Visual Studio 2017 or newer.

2. Set the **WebApplication** project as the default, if it isn't already.

3. Run and test one of the 3 demo pages

4. Look at the `Startup.cs` file for configuration to copy to your own project to use with the NuGet package.


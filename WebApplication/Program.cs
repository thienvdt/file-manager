﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Pchp.Library;

namespace WebApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseIISIntegration().UseKestrel(opt=> { 
                    opt.Limits.MaxRequestBodySize = null;
                    }).UseContentRoot(System.IO.Directory.GetCurrentDirectory()).UseStartup<Startup>();
                });
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplication.Models;
using WebApplication.Models.AccountViewModels;
using WebApplication.Services;
using Microsoft.Extensions.Configuration;

namespace WebApplication.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        public IConfiguration _configuration { get; }
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        public AccountController(

            IEmailSender emailSender,
            ILogger<AccountController> logger,
            IConfiguration config
            )
        {
            _configuration = config;
            _emailSender = emailSender;
            _logger = logger;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            ViewData["IsLoggedIn"] = "false";
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            // username = anet  
            var users = new ApplicationUsers(_configuration);
            var allUsers = users.GetUsers().FirstOrDefault();
            if (users.GetUsers().Any(u => u.UserName == model.UserName && u.Password== model.Password))
            {
                var userClaims = new List<Claim>()
                {
                new Claim(ClaimTypes.Name, model.UserName),
                 };
                model.IsLoggedIn = true;
                var grandmaIdentity = new ClaimsIdentity(userClaims, "User Identity");

                var userPrincipal = new ClaimsPrincipal(new[] { grandmaIdentity });
                await HttpContext.SignInAsync(userPrincipal);
                ViewData["IsLoggedIn"] = "true";

                return RedirectToAction("Index", "Home");
            }
            LoginViewModel viewModel = new LoginViewModel
            {
            IsLoggedIn=false,
            UserName= model.UserName
            };
            ViewData["IsLoggedIn"] = "false";
            ViewData["Status"] = "Đăng nhập không thành công, vui lòng thử lại";
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(AccountController.Login), "Account");
        }
    }


}


<?php

$config = include 'config/config.php';

//TODO switch to array
extract($config, EXTR_OVERWRITE);

include 'include/utils.php';
$name = $_POST['name'];

if ($_SESSION['RF']["verify"] != "RESPONSIVEfilemanager")
{
	response('forbiden', 403)->send();
	//exit;
}

include 'include/mime_type_lib.php';

if (
	strpos($_POST['path'], '/') === 0
	|| strpos($_POST['path'], '../') !== false
	|| strpos($_POST['path'], './') === 0
)
{
	response('wrong path', 400)->send();
	//exit;
}


if (strpos($name, '/') !== false)
{
	response('wrong path', 400)->send();
	//exit;
}

$path = $current_path . $_POST['path'];


$info = pathinfo($name);


if ( ! in_array(fix_strtolower($info['extension']), $ext))
{
	response('wrong extension', 400)->send();
	//exit;
}

if ( ! file_exists($path . $name))
{
	response('File not found', 404)->send();
	//exit;
}

$img_size = (string) (filesize($path . $name)); // Get the image size as string

$mime_type = get_file_mime_type($path . $name); // Get the correct MIME type depending on the file.
if(preg_match('/[^\x20-\x7e]/', $name)){
	$name = urlencode($name);
}

response(file_get_contents($path . $name), 200, array(
	'Pragma'              => 'private',
	'Cache-control'       => 'private, must-revalidate',
	'Content-Type'        => $mime_type,
	'Content-Length'      => $img_size,
	'Content-Disposition' => 'attachment; filename="' . ($name) . '"'
))->send();

//exit;
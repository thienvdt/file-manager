﻿using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Pchp.Core;
using WebApplication.Models;
using WebApplication.Options;
using WebApplication.Services;

namespace WebApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            HostEnvironment = environment;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment HostEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddAuthentication("CookieAuthentication")
                 .AddCookie("CookieAuthentication", config =>
                 {
                     config.ExpireTimeSpan = TimeSpan.FromDays(1);
                     config.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Strict;
                     config.Cookie.Name = "UserLoginCookie";
                     config.LoginPath = "/Account/Login";
                     config.LogoutPath = "/Account/Logout";
                 });

            services.AddControllersWithViews();
            // Adds a default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();
            services.Configure<IISServerOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });


            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
              
            });
            services.Configure<FormOptions>(options =>
            {
                options.MultipartBodyLengthLimit = int.MaxValue;
            });
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.Cookie.HttpOnly = true;
            });

            services.AddControllersWithViews();
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            app.UseSession();

            var rfmOptions = new ResponsiveFileManagerOptions();
            Configuration.GetSection("ResponsiveFileManagerOptions").Bind(rfmOptions);

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UsePhp(new PhpRequestOptions(scriptAssemblyName: "ResponsiveFileManager")
            {
                BeforeRequest = (Context ctx) =>
                {
                    ctx.Globals["appsettings"] = new PhpArray
                    {
                        { "upload_dir", rfmOptions.UploadDirectory },
                        { "current_path", rfmOptions.CurrentPath },
                        { "thumbs_base_path", rfmOptions.ThumbsBasePath },
                        { "MaxSizeUpload", rfmOptions.MaxSizeUpload } //TODO: Test this new option
                    };
                }
            });

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = HostEnvironment.WebRootFileProvider,
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
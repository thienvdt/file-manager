﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace WebApplication.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUsers 
    {
        public IConfiguration Configuration { get; }

        public ApplicationUsers(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public int Id { get; set; }
        List<ApplicationUsers> lstUser { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public IEnumerable<ApplicationUsers> GetUsers()
        {
            return new List<ApplicationUsers>() { new ApplicationUsers(Configuration) { Id = 101, UserName = Configuration["SiteUsername"], Name = Configuration["SiteUsername"], EmailId = "", Password = Configuration["SitePassword"] } };
        }
    }
}
